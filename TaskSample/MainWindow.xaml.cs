﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TaskSample {
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window {
        public MainWindow() {
            InitializeComponent();
        }
        /// <summary>
        /// 指定されたアドレスのテキストを取得します
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        private static async Task<string> GetRemoteText(string address) {
            using (var wc = new WebClient() { Encoding = Encoding.UTF8 }) {
                var data = await wc.DownloadStringTaskAsync(address);
                return data;
            }
        }
        /// <summary>
        /// 一定時間ごとに指定したアドレスのテキストを取得します
        /// </summary>
        /// <param name="token"></param>
        /// <param name="progress"></param>
        /// <param name="address"></param>
        /// <param name="intervalMs"></param>
        /// <returns></returns>
        private static async Task GetRemoteTextContinuous(CancellationToken token, IProgress<string> progress, string address, int intervalMs) {
            while (!token.IsCancellationRequested) {
                var data = await GetRemoteText(address);
                progress.Report(data);

                await Task.Delay(intervalMs);
            }
        }


        CancellationTokenSource cancelToken = null;
        private async void Button_Click(object sender, RoutedEventArgs e) {
            if (cancelToken != null) {
                cancelToken.Cancel();
                return;
            }

            var address = "https://now.httpbin.org/";
            var progress = new Progress<string>(data => {
                this.dataTextBox.Text = data;
                this.statusLabel.Content = $"{DateTime.Now} {address}";
            });
            cancelToken = new CancellationTokenSource();
            await GetRemoteTextContinuous(
                    token: cancelToken.Token,
                    progress: progress,
                    address: address,
                    intervalMs: 1000
                );
            this.statusLabel.Content = $"{DateTime.Now} Task Canceled";
            cancelToken.Dispose();
            cancelToken = null;

        }
    }
}
